﻿using System;

namespace TemaGhicesteNumarul
{
    class Program
    {
        static void Main(string[] args)
        {
            //generez numr random intr 0 si 100
            //var de baza
            Random rand = new Random();
            int numRandom = rand.Next(100);
            int numIncercari = 0;
            int numUser = int.MaxValue;
            //cliclu pentru operatii
            while (numUser != numRandom)
            {
                Console.WriteLine("Introduceti numar");
                numUser = int.Parse(Console.ReadLine());
                //diferenta intre numarUser si numarRnandom
                int temp = Math.Abs(numUser - numRandom);
                //increment numarIncercari
                numIncercari++;
                //statement de control
                if (temp <= 3)
                {
                    Console.WriteLine("Foarte fierbinte");
                }else if (temp <= 5)
                {
                    Console.WriteLine("Fierbinte");
                }else if (temp <= 10)
                {
                    Console.WriteLine("Cald");
                }else if (temp <= 50)
                {
                    Console.WriteLine("Rece");
                }
                else
                {
                    Console.WriteLine("Foarte rece");
                }
            }
            //afisez numarul de incercari
            Console.WriteLine("Felicitari,ai ghicit numarul in" + " " + numIncercari+" "+"incercari");
        }
    }
}
